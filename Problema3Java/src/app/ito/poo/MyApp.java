package app.ito.poo;
import clases.ito.poo.CuerpoCeleste;
import clases.ito.poo.Ubicacion;

public class MyApp {
	
	static void run() {
		CuerpoCeleste c1 = new CuerpoCeleste("Sol", "Gaseoso");
		Ubicacion l1 = new Ubicacion(1363, 8920, "Solsticio de verano", 15123.15F);
		c1.addLocalizaciones(l1);
		System.out.println(c1);
	}

	public static void main(String[] args) {
		run();
	}

}
