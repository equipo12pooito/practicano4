package clases.ito.poo;
import java.util.ArrayList;

public class CuerpoCeleste {

	private String nombre;
	private ArrayList<Ubicacion> localizaciones = new ArrayList<Ubicacion>();
	private String composicion;
	
	public CuerpoCeleste() {
		super();
	}
	
	public CuerpoCeleste(String nombre, String composicion) {
		super();
		this.nombre = nombre;
		this.composicion = composicion;
	}

	//===============================
	
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public ArrayList<Ubicacion> getLocalizaciones() {
		return localizaciones;
	}

	public void addLocalizaciones(Ubicacion localizaciones) {
		this.localizaciones.add(localizaciones);
	}

	public String getComposicion() {
		return composicion;
	}

	public void setComposicion(String composicion) {
		this.composicion = composicion;
	}

	//===============================

	public float desplazamiento(int i, int j) {
		float a = 0;
		return a;
	}

	@Override
	public String toString() {
		return "Cuerpo Celeste: " + nombre + "\nLocalizaciones: " + localizaciones + "\nComposicion: " + composicion;
	}

}
