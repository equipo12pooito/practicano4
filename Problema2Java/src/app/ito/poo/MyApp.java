package app.ito.poo;
import clases.ito.poo.Fruta;
import clases.ito.poo.Periodo;

public class MyApp {
	
	static void run() {
		Fruta f1 = new Fruta("Manzana", 3.5F, 2, 5);
		System.out.println(f1);
		System.out.println();
		Periodo p1 = new Periodo("Un mes", 4000);
		f1.agregarPeriodo(p1);
		System.out.println(f1);
	}

	public static void main(String[] args) {
		run();
	}

}
