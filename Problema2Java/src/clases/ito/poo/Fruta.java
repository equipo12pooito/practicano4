package clases.ito.poo;
import java.util.ArrayList;

public class Fruta {

	private String nombre;
	private float extension;
	private float costoPromedio;
	private float precioVentaProm;
	private ArrayList<Periodo> periodos = new ArrayList<Periodo>();

	//=============================
	
	public Fruta() {
		super();
	}

	public Fruta(String nombre, float extension, float costoPromedio, float precioVentaProm) {
		super();
		this.nombre = nombre;
		this.extension = extension;
		this.costoPromedio = costoPromedio;
		this.precioVentaProm = precioVentaProm;
	}

	//=============================

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public float getExtension() {
		return extension;
	}

	public void setExtension(float extension) {
		this.extension = extension;
	}

	public float getCostoPromedio() {
		return costoPromedio;
	}

	public void setCostoPromedio(float costoPromedio) {
		this.costoPromedio = costoPromedio;
	}

	public float getPrecioVentaProm() {
		return precioVentaProm;
	}

	public void setPrecioVentaProm(float precioVentaProm) {
		this.precioVentaProm = precioVentaProm;
	}

	public ArrayList<Periodo> getPeriodos() {
		return this.periodos;
	}
	
	//=============================

	public void agregarPeriodo(Periodo p) {
		this.periodos.add(p);
	}

	public boolean eliminarPeriodo(int i) {
		boolean e;
		if (i > this.periodos.size() || i < 0)
			e = false;
		else {
			this.periodos.remove(i);
			e = true;
		}
		return e;
	}

	@Override
	public String toString() {
		return "Fruta: " + nombre + "\nExtension: " + extension + "\nCosto Promedio: " + costoPromedio
				+ "\nPrecio Venta Promedio: " + precioVentaProm + "\nPeriodos: " + periodos;
	}
	
}
