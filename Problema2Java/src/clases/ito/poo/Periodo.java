package clases.ito.poo;

public class Periodo {

	private String tiempoCosecha;
	private float cantCosechaxtiempo;
	
	public Periodo() {
		super();
	}
	
	public Periodo(String tiempoCosecha, float cantCosechaxtiempo) {
		super();
		this.tiempoCosecha = tiempoCosecha;
		this.cantCosechaxtiempo = cantCosechaxtiempo;
	}

	//=============================

	public String getTiempoCosecha() {
		return tiempoCosecha;
	}

	public void setTiempoCosecha(String tiempoCosecha) {
		this.tiempoCosecha = tiempoCosecha;
	}

	public float getCantCosechaxtiempo() {
		return cantCosechaxtiempo;
	}

	public void setCantCosechaxtiempo(float cantCosechaxtiempo) {
		this.cantCosechaxtiempo = cantCosechaxtiempo;
	}

	//=============================
	
	public float costoPeriodo() {
		float i = 0;
		return i;
	}

	public float gananciaEstimada() {
		float i = 0;
		return i;
	}

	@Override
	public String toString() {
		return "(Tiempo de cosecha: " + tiempoCosecha + ", Cantidad de cosecha: " + cantCosechaxtiempo + ")";
	}
	
}