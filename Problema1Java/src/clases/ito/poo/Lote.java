package clases.ito.poo;
import java.time.LocalDate;

public class Lote {

	private int numLote;
	private int numPiezas;
	private LocalDate fecha;
	
	public Lote() {
		super();
	}
	
	public Lote(int numLote, int numPiezas, LocalDate fecha) {
		super();
		this.numLote = numLote;
		this.numPiezas = numPiezas;
		this.fecha = fecha;
	}

	//=============================

	public int getNumLote() {
		return numLote;
	}

	public void setNumLote(int numLote) {
		this.numLote = numLote;
	}

	public int getNumPiezas() {
		return numPiezas;
	}

	public void setNumPiezas(int numPiezas) {
		this.numPiezas = numPiezas;
	}

	public LocalDate getFecha() {
		return fecha;
	}

	public void setFecha(LocalDate fecha) {
		this.fecha = fecha;
	}
	
	//=============================

	public float costoProduccion() {
		float i=0;
		return i;
	}

	public float montoProduccionxlote() {
		float i=0;
		return i;
	}

	public float montoRecuperacionxpieza() {
		float i=0;
		return i;
	}

	@Override
	public String toString() {
		return "(Numero de lote: " + numLote + ", Numero de piezas: " + numPiezas + ", Fecha: " + fecha + ")";
	}
	
	
}
