package clases.ito.poo;
import java.time.LocalDate;
import java.util.ArrayList;

public class Prenda {

	private int modelo;
	private String tela;
	private float costoProduccion;
	private String genero;
	private String temporada;
	private ArrayList<Lote> lotes = new ArrayList<Lote>();
	
	public Prenda() {
		super();
	}
	
	public Prenda(int modelo, String tela, float costoProduccion, String genero, String temporada) {
		super();
		this.modelo = modelo;
		this.tela = tela;
		this.costoProduccion = costoProduccion;
		this.genero = genero;
		this.temporada = temporada;
	}

	//====================================
	
	public int getModelo() {
		return modelo;
	}

	public void setModelo(int modelo) {
		this.modelo = modelo;
	}

	public String getTela() {
		return tela;
	}

	public void setTela(String tela) {
		this.tela = tela;
	}

	public float getCostoProduccion() {
		return costoProduccion;
	}

	public void setCostoProduccion(float costoProduccion) {
		this.costoProduccion = costoProduccion;
	}

	public String getGenero() {
		return genero;
	}

	public void setGenero(String genero) {
		this.genero = genero;
	}

	public String getTemporada() {
		return temporada;
	}

	public void setTemporada(String temporada) {
		this.temporada = temporada;
	}
	
	public ArrayList<Lote> getLote(){
		return this.lotes;
	}
	
	public void addLote(int i, int j, LocalDate k) {
		Lote e = new Lote(i, j, k);
		this.lotes.add(e);
	}
	
	//============================================

	public float costoxLote(float costoxUnidad) {
		float i = 0;
		return i;
	}

	@Override
	public String toString() {
		return "Modelo " + modelo + "\nTela: " + tela + "\nCosto Produccion: " + costoProduccion + "\nGenero: "
				+ genero + "\nTemporada: " + temporada + "\nLotes: " + lotes;
	}
	
}
